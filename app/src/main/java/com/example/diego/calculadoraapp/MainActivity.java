package com.example.diego.calculadoraapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    private String aux=null, aux2=null;
    private boolean banderaoperacion=false;
    private char signon=0;
    private boolean banderapunto=false;
    //private boolean banderaparentesis=false;
    private calculadorsilla1 C=new calculadorsilla1();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText jTextField1 = (EditText)findViewById(R.id.txtDisplay);
        final Button btn1 = (Button)findViewById(R.id.btn1);
        final Button btn2 = (Button)findViewById(R.id.btn2);
        final Button btn3 = (Button)findViewById(R.id.btn3);
        final Button btn4 = (Button)findViewById(R.id.btn4);
        final Button btn5 = (Button)findViewById(R.id.btn5);
        final Button btn6 = (Button)findViewById(R.id.btn6);
        final Button btn7 = (Button)findViewById(R.id.btn7);
        final Button btn8 = (Button)findViewById(R.id.btn8);
        final Button btn9 = (Button)findViewById(R.id.btn9);
        final Button btn0 = (Button)findViewById(R.id.btn0);
        final Button btnpunto = (Button)findViewById(R.id.btnpunto);
        final Button btnigual = (Button)findViewById(R.id.btnigual);
        final Button btnsuma = (Button)findViewById(R.id.btnsuma);
        final Button btnmenos = (Button)findViewById(R.id.btnmenos);
        final Button btnpor = (Button)findViewById(R.id.btnpor);
        final Button btndivision = (Button)findViewById(R.id.btndivision);
        final Button btna = (Button)findViewById(R.id.a);
        final Button btnb = (Button)findViewById(R.id.b);
        final Button btnc = (Button)findViewById(R.id.c);
        final Button btnd = (Button)findViewById(R.id.d);
        final Button btne = (Button)findViewById(R.id.e);
        final Button btnf = (Button)findViewById(R.id.f);
        final Button potenciar = (Button)findViewById(R.id.potenciar);
        final Button backspace = (Button)findViewById(R.id.backspace);
        final RadioButton normalmode=(RadioButton)findViewById(R.id.normalmode);
        final RadioButton escriturahexadecimal =(RadioButton)findViewById(R.id.hexmode);

        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    Log.d("calculadora","I am here");
                    if(aux==null)
                        aux="1";
                    else
                        aux=aux+"1";
                    jTextField1.setText(aux);
                }
            }
        });
        btn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="2";
                    else
                        aux=aux+"2";
                    jTextField1.setText(aux);
                }
            }
        });
        btn3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="3";
                    else
                        aux=aux+"3";
                    jTextField1.setText(aux);
                }
            }
        });
        btn4.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="4";
                    else
                        aux=aux+"4";
                    jTextField1.setText(aux);
                }
            }
        });
        btn5.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="5";
                    else
                        aux=aux+"5";
                    jTextField1.setText(aux);
                }
            }
        });
        btn6.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="6";
                    else
                        aux=aux+"6";
                    jTextField1.setText(aux);
                }
            }
        });
        btn7.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="7";
                    else
                        aux=aux+"7";
                    jTextField1.setText(aux);
                }
            }
        });
        btn8.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="8";
                    else
                        aux=aux+"8";
                    jTextField1.setText(aux);
                }
            }
        });
        btn9.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="9";
                    else
                        aux=aux+"9";
                    jTextField1.setText(aux);
                }
            }
        });
        btn0.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true|| escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="0";
                    else
                        aux=aux+"0";
                    jTextField1.setText(aux);
                }
            }
        });
        btna.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="A";
                    else
                        aux=aux+"A";
                    jTextField1.setText(aux);
                }
            }
        });
        btnb.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="B";
                    else
                        aux=aux+"B";
                    jTextField1.setText(aux);
                }
            }
        });
        btnc.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="C";
                    else
                        aux=aux+"C";
                    jTextField1.setText(aux);
                }
            }
        });
        btnd.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="D";
                    else
                        aux=aux+"D";
                    jTextField1.setText(aux);
                }
            }
        });
        btne.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="E";
                    else
                        aux=aux+"E";
                    jTextField1.setText(aux);
                }
            }
        });
        btnf.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(escriturahexadecimal.isChecked()==true)
                {
                    if(aux==null)
                        aux="F";
                    else
                        aux=aux+"F";
                    jTextField1.setText(aux);
                }
            }
        });
        btnpunto.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true)
                {
                    if(aux!=null)
                    {
                        if(!banderapunto)
                        {
                            aux=aux+".";
                            jTextField1.setText(aux);
                            banderapunto=true;// TODO add your handling code here:
                        }
                    }
                }
            }
        });
        potenciar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true)
                {
                    jTextField1.setText(null);
                    if(aux!=null && aux!="")
                    {
                        jTextField1.setText(aux+"^");
                        aux2=aux;
                    }
                    else // si ya tenia una cantidad guardada pero quiero cambiar de signo, lo cambio
                    {
                        if(aux2!=null && aux!="")
                        {
                            jTextField1.setText(aux2+"^");
                        }

                    }
                    if(signon==0)
                        aux=null;
                    banderapunto=false;
                    signon='^';

                }
            }
        });
        backspace.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cad2="";

                String cad = jTextField1.getText().toString();
                if(cad!="" && cad!=null)
                {
                    if(cad.length()>0)
                        cad2=cad.substring(0,cad.length()-1);
                    else
                        cad2="";
                }

                aux=cad2;
                jTextField1.setText(cad2);// TODO add your handling code here:
            }
        });
        btnsuma.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked()==true)
                {
                    jTextField1.setText(null);
                    if(aux!=null && aux!="")
                    {
                        jTextField1.setText(aux+"+");
                        aux2=aux;
                        aux="";
                    }
                    else // si ya tenia una cantidad guardada pero quiero cambiar de signo, lo cambio
                    {
                        if(aux2!=null && aux!="")
                        {
                            jTextField1.setText(aux2+"+");
                        }

                    }
                    if(signon==0)
                        aux=null;
                    banderapunto=false;
                    signon='+';

                }
            }
        });
        btnmenos.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked())
                {
                    jTextField1.setText(null);
                    if(aux!=null && aux!="")
                    {
                        jTextField1.setText(aux+"-");
                        aux2=aux;
                        aux="";
                    }
                    else // si ya tenia una cantidad guardada pero quiero cambiar de signo, lo cambio
                    {
                        if(aux2!=null && aux!="")
                        {
                            jTextField1.setText(aux2+"-");
                        }

                    }
                    if(signon==0)
                        aux=null;
                    banderapunto=false;
                    signon='-';

                }
            }
        });
        btnpor.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked())
                {
                    jTextField1.setText(null);
                    if(aux!=null && aux!="")
                    {
                        jTextField1.setText(aux+"x");
                        aux2=aux;
                        aux="";
                    }
                    else // si ya tenia una cantidad guardada pero quiero cambiar de signo, lo cambio
                    {
                        if(aux2!=null && aux!="")
                        {
                            jTextField1.setText(aux2+"x");
                        }

                    }
                    if(signon==0)
                        aux=null;
                    banderapunto=false;
                    signon='x';

                }
            }
        });
        btndivision.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(normalmode.isChecked())
                {
                    jTextField1.setText(null);
                    if(aux!=null && aux!="")
                    {
                        jTextField1.setText(aux+"/");
                        aux2=aux;
                        aux="";
                    }
                    else // si ya tenia una cantidad guardada pero quiero cambiar de signo, lo cambio
                    {
                        if(aux2!=null && aux!="")
                        {
                            jTextField1.setText(aux2+"/");
                        }

                    }
                    if(signon==0)
                        aux=null;
                    banderapunto=false;
                    signon='/';

                }
            }
        });
        btnigual.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(signon!=0 && aux!=null && aux2!=null && aux2!="" && aux!="" && normalmode.isChecked()==true)
                {
                    banderaoperacion=false;
                    aux=C.Operacion(Double.parseDouble(aux2), signon, Double.parseDouble(aux));
                    jTextField1.setText(aux);
                    aux2=null;
                    banderapunto=true;
                    signon=0;
                }
            }
        });
    }
}
